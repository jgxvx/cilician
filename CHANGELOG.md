# Changelog

## 4.0.0 - 2023-12-25
* Adding support for PHP 8.3
* Removing support for PHP 8.1
* Improving error handling, introducing `InvalidDateException` and `ResultException`

## 3.0.1 - 2023-08-08
* Upgrading PHPUnit to 10.3

## 3.0.0 - 2023-08-08
* Migrating to PHP 8.2

## 2.0.1 - 2022-05-13
* Fixing cache key generation for null filter on breaches call

## 2.0.0 - 2022-05-13
* Increasing minimum required PHP version to 8.1
* Migrating to Have I Been Pwned API v3
