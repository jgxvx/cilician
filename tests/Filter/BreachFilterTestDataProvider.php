<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Filter;

class BreachFilterTestDataProvider
{
    public static function getDomains(): array
    {
        return [
            ['adobe.com', true, false],
            ['adobe.com', false, true],
        ];
    }
}
