<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Filter;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Filter\BreachFilter
 */
class BreachFilterTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Filter\BreachFilterTestDataProvider::getDomains()
     */
    public function testIfInitializationWorks(string $domain, bool $includeUnverified): void
    {
        $filter = new BreachFilter(['domain' => $domain, 'includeUnverified' => $includeUnverified]);

        $this->assertEquals($domain, $filter->getDomain());
        $this->assertEquals($includeUnverified, $filter->getIncludeUnverified());
    }

    public function testDefaultValuesAreCorrect(): void
    {
        $filter = new BreachFilter();

        $this->assertNull($filter->getDomain());
        $this->assertFalse($filter->getIncludeUnverified());
    }
}
