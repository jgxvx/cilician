<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Model\BreachModel;
use Jgxvx\Cilician\Model\PasteModel;
use Jgxvx\Cilician\Result\BreachResult;
use Jgxvx\Cilician\Result\PasswordCheckResult;
use Jgxvx\Cilician\Result\PasteResult;

class CilicianTestDataProvider
{
    public static function getPasswordCheckResults(): array
    {
        $password1 = '12345678';
        $result1   = new PasswordCheckResult($password1, 2840404);

        $password2 = 'notpwned';
        $result2   = new PasswordCheckResult($password2);

        return [
            [$password1, $result1],
            [$password2, $result2],
        ];
    }

    public static function getBreachedSites(): array
    {
        $adobe = self::getAdobeBreachModel();

        $filter1 = new BreachFilter();
        $filter1->setDomain('adobe.com');

        $result1 = new BreachResult([$adobe]);

        return [
            [$filter1, $result1],
        ];
    }

    public static function getBreachedSite(): array
    {
        $name1 = 'Adobe';

        $result1 = new BreachResult([self::getAdobeBreachModel()]);

        return [
            [$name1, $result1],
        ];
    }

    public static function getBreachesForAccount(): array
    {
        $adobe   = self::getAdobeBreachModel();
        $spambot = self::getOnlineSpambotBreachModel();

        $account1 = 'john@doe.com';
        $filter1  = null;
        $result1  = new BreachResult([$adobe, $spambot]);

        return [
            [$account1, $filter1, $result1],
        ];
    }

    public static function getPastesForAccount(): array
    {
        $pastebin = static::getPastebinPasteModel();
        $pastie   = static::getPastiePasteModel();

        $account1 = 'john@doe.com';
        $result1  = new PasteResult([$pastebin, $pastie]);

        $account2 = 'jane@doe.com';
        $result2  = new PasteResult([]);

        return [
            'compromised' => [$account1, $result1],
            'not found'   => [$account2, $result2],
        ];
    }

    public static function getAdobeBreachModel(): BreachModel
    {
        return BreachModel::fromArray([
            'Name'         => 'Adobe',
            'Title'        => 'Adobe',
            'Domain'       => 'adobe.com',
            'BreachDate'   => '2013-10-04',
            'AddedDate'    => '2013-12-04T00:00Z',
            'ModifiedDate' => '2013-12-04T00:00Z',
            'PwnCount'     => 152445165,
            'Description'  => 'In October 2013, 153 million Adobe accounts were breached with each containing an internal ID, username, email, <em>encrypted</em> password and a password hint in plain text. The password cryptography was poorly done and <a href="http://stricture-group.com/files/adobe-top100.txt" target="_blank" rel="noopener">many were quickly resolved back to plain text</a>. The unencrypted hints also <a href="http://www.troyhunt.com/2013/11/adobe-credentials-and-serious.html" target="_blank" rel="noopener">disclosed much about the passwords</a> adding further to the risk that hundreds of millions of Adobe customers already faced.',
            'DataClasses'  => ['Email addresses', 'Password hints', 'Passwords', 'Usernames'],
            'IsVerified'   => true,
            'IsSensitive'  => false,
            'IsRetired'    => false,
            'IsSpamList'   => false,
            'IsFabricated' => true,
        ]);
    }

    public static function getOnlineSpambotBreachModel(): BreachModel
    {
        return BreachModel::fromArray([
            'Title'        => 'Onliner Spambot',
            'Name'         => 'OnlinerSpambot',
            'Domain'       => '',
            'BreachDate'   => '2017-08-28',
            'AddedDate'    => '2017-08-29T19:25:56Z',
            'ModifiedDate' => '2017-08-29T19:25:56Z',
            'PwnCount'     => 711477622,
            'Description'  => 'In August 2017, a spambot by the name of <a href="https://benkowlab.blogspot.com.au/2017/08/from-onliner-spambot-to-millions-of.html" target="_blank" rel="noopener">Onliner Spambot was identified by security researcher Benkow moʞuƎq</a>. The malicious software contained a server-based component located on an IP address in the Netherlands which exposed a large number of files containing personal information. In total, there were 711 million unique email addresses, many of which were also accompanied by corresponding passwords. A full write-up on what data was found is in the blog post titled <a href="https://www.troyhunt.com/inside-the-massive-711-million-record-onliner-spambot-dump" target="_blank" rel="noopener">Inside the Massive 711 Million Record Onliner Spambot Dump</a>.',
            'DataClasses'  => [
                'Email addresses',
                'Passwords',
            ],
            'IsVerified'   => true,
            'IsFabricated' => false,
            'IsSensitive'  => false,
            'IsActive'     => true,
            'IsRetired'    => false,
            'IsSpamList'   => true,
            'LogoType'     => 'png',
        ]);
    }

    public static function getPastebinPasteModel(): PasteModel
    {
        return PasteModel::fromArray([
            'Source'     => 'Pastebin',
            'Id'         => '8Q0BvKD8',
            'Title'      => 'syslog',
            'Date'       => '2014-03-04T19:14:54Z',
            'EmailCount' => 139,
        ]);
    }

    public static function getPastiePasteModel(): PasteModel
    {
        return PasteModel::fromArray([
            'Source'     => 'Pastie',
            'Id'         => '7152479',
            'Date'       => '2013-03-28T16:51:10Z',
            'EmailCount' => 30,
        ]);
    }
}
