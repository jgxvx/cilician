<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use Jgxvx\Cilician\Mock\Service\HttpClientMock;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Service\PwnedPasswordsApiClientAwareTrait
 */
class PwnedPasswordsApiClientAwareTraitTest extends TestCase
{
    /** @var PwnedPasswordsApiClientAwareTrait */
    private $trait;

    protected function setUp(): void
    {
        parent::setUp();
        $this->trait = $this->createMockForTrait();
    }

    protected function tearDown(): void
    {
        $this->trait = null;
        parent::tearDown();
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * TESTS
     */

    public function testIfServiceIsSet(): void
    {
        $service = new PwnedPasswordsApiClient(HttpClientMock::get(), 'test-api-key');
        $this->trait->setPwnedPasswordsApiClient($service);

        $this->assertEquals($service, $this->trait->getPwnedPasswordsApiClient());
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * HELPERS & MOCKS
     */

    /**
     * @throws \ReflectionException
     *
     * @return \PHPUnit\Framework\MockObject\MockObject|PwnedPasswordsApiClientAwareTrait
     */
    private function createMockForTrait()
    {
        return $this->getMockForTrait(PwnedPasswordsApiClientAwareTrait::class);
    }
}
