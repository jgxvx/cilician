<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use GuzzleHttp\Exception\GuzzleException;
use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Mock\Service\HttpClientMock;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Service\HaveIBeenPwnedApiClient
 */
class HaveIBeenPwnedApiClientTest extends TestCase
{
    private ?HaveIBeenPwnedApiClient $service = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->createService();
    }

    protected function tearDown(): void
    {
        $this->service = null;
        parent::tearDown();
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * TESTS
     */

    /**
     * @dataProvider \Jgxvx\Cilician\Service\HaveIBeenPwnedApiClientTestDataProvider::getBreachesForAccount()
     */
    public function testIfBreachesAreFoundForAccount(string $account, BreachFilter $filter = null, array $expectedNames = []): void
    {
        try {
            $response = $this->service->getBreachesForAccount($account, $filter);
        } catch (GuzzleException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }

        self::assertEquals(200, $response->getStatusCode());

        $body  = $response->getBody()->getContents();
        $array = \json_decode($body, true);
        $count = \count($array);

        self::assertCount(\count($expectedNames), $array);

        for ($i = 0; $i < $count; ++$i) {
            self::assertEquals($expectedNames[$i], $array[$i]['Name']);
        }
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Service\HaveIBeenPwnedApiClientTestDataProvider::getBreachedSites()
     */
    public function testIfBreachedSitesAreFound(BreachFilter $filter = null, array $expectedNames = []): void
    {
        try {
            $response = $this->service->getBreachedSites($filter);
        } catch (GuzzleException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }

        self::assertEquals(200, $response->getStatusCode());

        $body  = $response->getBody()->getContents();
        $array = \json_decode($body, true);
        $count = \count($array);

        self::assertCount(\count($expectedNames), $array);

        for ($i = 0; $i < $count; ++$i) {
            self::assertEquals($expectedNames[$i], $array[$i]['Name']);
        }
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Service\HaveIBeenPwnedApiClientTestDataProvider::getBreachedSite()
     */
    public function testIfBreachedSiteIsFound(string $name, array $expected = null): void
    {
        try {
            $response = $this->service->getBreachedSite($name);
        } catch (GuzzleException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }

        self::assertEquals(200, $response->getStatusCode());

        $body   = $response->getBody()->getContents();
        $actual = \json_decode($body, true);

        self::assertEquals($expected, $actual);
    }

    public function testIfDataClassesAreFound(): void
    {
        try {
            $response = $this->service->getDataClasses();
        } catch (GuzzleException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }

        self::assertEquals(200, $response->getStatusCode());

        $body   = $response->getBody()->getContents();
        $actual = \json_decode($body, true);

        self::assertNotEmpty($actual);

        foreach ($actual as $dataClass) {
            self::assertIsString($dataClass);
        }
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * HELPERS & MOCKS
     */

    private function createService(): HaveIBeenPwnedApiClient
    {
        return new HaveIBeenPwnedApiClient(HttpClientMock::get(), 'test-api-key');
    }
}
