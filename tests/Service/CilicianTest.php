<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use Jgxvx\Cilician\Exception\ApiException;
use Jgxvx\Cilician\Exception\AuthorizationException;
use Jgxvx\Cilician\Exception\ConnectionException;
use Jgxvx\Cilician\Exception\GeneralClientException;
use Jgxvx\Cilician\Exception\RateLimitException;
use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Mock\CacheMock;
use Jgxvx\Cilician\Mock\Service\HttpClientMock;
use Jgxvx\Cilician\Result\BreachResult;
use Jgxvx\Cilician\Result\PasswordCheckResult;
use Jgxvx\Cilician\Result\PasteResult;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Service\Cilician
 */
class CilicianTest extends TestCase
{
    private ?Cilician $service = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->createService();
    }

    protected function tearDown(): void
    {
        $this->service = null;
        parent::tearDown();
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * TESTS
     */

    /**
     * @dataProvider \Jgxvx\Cilician\Service\CilicianTestDataProvider::getPasswordCheckResults()
     */
    public function testIfPasswordsAreChecked(string $password, PasswordCheckResult $expected): void
    {
        $actual = $this->service->checkPassword($password);

        self::assertEquals($expected, $actual);
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Service\CilicianTestDataProvider::getBreachedSites()
     */
    public function testIfBreachedSitesAreFound(BreachFilter $filter = null, BreachResult $expected = null): void
    {
        $actual = $this->service->getBreachedSites($filter);
        self::assertEquals($expected, $actual);
    }

    public function testIfBreachedSitesAreLoadedFromCache(): void
    {
        $firstResult  = $this->service->getBreachedSites(null);
        $secondResult = $this->service->getBreachedSites(null);

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());

        $service = $this->createCachedService();

        $firstResult  = $service->getBreachedSites(null);
        $secondResult = $service->getBreachedSites(null);

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());

        $filter       = new BreachFilter(['domain' => 'adobe.com']);
        $firstResult  = $service->getBreachedSites($filter);
        $secondResult = $service->getBreachedSites($filter);

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Service\CilicianTestDataProvider::getBreachedSite()
     */
    public function testIfBreachedSiteIsFound(string $name, BreachResult $expected): void
    {
        $actual = $this->service->getBreachedSite($name);

        self::assertEquals($expected, $actual);
    }

    public function testIfBreachedSiteIsLoadedFromCache(): void
    {
        $firstResult  = $this->service->getBreachedSite('Adobe');
        $secondResult = $this->service->getBreachedSite('Adobe');

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());

        $service = $this->createCachedService();

        $firstResult  = $service->getBreachedSite('Adobe');
        $secondResult = $service->getBreachedSite('Adobe');

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());

        $firstResult  = $service->getBreachedSite('Google');
        $secondResult = $service->getBreachedSite('Google');

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Service\CilicianTestDataProvider::getBreachesForAccount()
     */
    public function testIfBreachesAreFoundForAccount(string $account, BreachFilter $filter = null, BreachResult $expected = null): void
    {
        $actual = $this->service->getBreachesForAccount($account, $filter);

        self::assertEquals($expected, $actual);
    }

    public function testIfBreachesAreLoadedFromCache(): void
    {
        $firstResult  = $this->service->getBreachesForAccount('john@doe.com');
        $secondResult = $this->service->getBreachesForAccount('john@doe.com');

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());

        $service = $this->createCachedService();

        $firstResult  = $service->getBreachesForAccount('john@doe.com');
        $secondResult = $service->getBreachesForAccount('john@doe.com');

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());
    }

    public function testIfDataClassesAreFound(): void
    {
        $result = $this->service->getDataClasses();

        self::assertNotEmpty($result->getDataClasses());

        foreach ($result->getDataClasses() as $dataClass) {
            self::assertIsString($dataClass);
        }
    }

    public function testIfDataClassesAreLoadedFromCache(): void
    {
        $firstResult  = $this->service->getDataClasses();
        $secondResult = $this->service->getDataClasses();

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());

        $service = $this->createCachedService();

        $firstResult  = $service->getDataClasses();
        $secondResult = $service->getDataClasses();

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());
    }

    public function testIfPasswordsAreLoadedFromCache(): void
    {
        $firstResult  = $this->service->checkPassword('12345678');
        $secondResult = $this->service->checkPassword('12345678');

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());

        $service = $this->createCachedService();

        $firstResult  = $service->checkPassword('12345678');
        $secondResult = $service->checkPassword('12345678');

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());

        $thirdResult = $service->checkPassword('notpwned');

        self::assertFalse($thirdResult->isCached());
    }

    public function testIfRateLimitIsExceeded(): void
    {
        $this->expectExceptionMessage('Rate limit exceeded. Wait for 2 second(s).');
        $this->expectException(RateLimitException::class);
        $this->service->checkPassword('ratelimit');
    }

    public function testIfRetryAfterHeaderIsDetected(): void
    {
        try {
            $this->service->checkPassword('ratelimit');
        } catch (RateLimitException $ex) {
            self::assertEquals(2, $ex->getDelay());
        }
    }

    public function testIfInvalidGetCacheKeyIsCaught(): void
    {
        $service = $this->createCachedService();

        $result = $service->checkPassword('exception-get');

        self::assertFalse($result->isCached());
    }

    public function testIfInvalidSetCacheKeyIsCaught(): void
    {
        $service = $this->createCachedService();

        $firstResult  = $service->checkPassword('exception-set');
        $secondResult = $service->checkPassword('exception-set');

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Service\CilicianTestDataProvider::getPastesForAccount()
     */
    public function testIfPastesAreFound(string $account, PasteResult $expected): void
    {
        $actual = $this->service->getPastesForAccount($account);

        self::assertEquals($expected, $actual);
    }

    public function testIfPastesAreLoadedFromCache(): void
    {
        $firstResult  = $this->service->getPastesForAccount('john@doe.com');
        $secondResult = $this->service->getPastesForAccount('john@doe.com');

        self::assertFalse($firstResult->isCached());
        self::assertFalse($secondResult->isCached());

        $service = $this->createCachedService();

        $firstResult  = $service->getPastesForAccount('john@doe.com');
        $secondResult = $service->getPastesForAccount('john@doe.com');

        self::assertFalse($firstResult->isCached());
        self::assertTrue($secondResult->isCached());
    }

    public function testIfUnknownClientExceptionIsThrownOnNullResponse(): void
    {
        $this->expectException(GeneralClientException::class);
        $this->service->getBreachesForAccount('UnknownClientException');
    }

    public function testIfNotFoundExceptionIsCaught(): void
    {
        $result = $this->service->getBreachesForAccount('NotFound');

        self::assertFalse($result->isPwned());
        self::assertCount(0, $result->getBreaches());
    }

    public function testIfAuthorizationErrorIsDetected(): void
    {
        $this->expectException(AuthorizationException::class);
        $this->service->getBreachesForAccount('NotAuthorized');
    }

    public function testIfGeneralClientFailureIsDetected(): void
    {
        $this->expectExceptionMessage('This is the message');
        $this->expectException(GeneralClientException::class);
        $this->service->getBreachesForAccount('GeneralClientException');
    }

    public function testIfServerExceptionIsHandled(): void
    {
        $this->expectExceptionMessage('An error occurred on the remote API server');
        $this->expectException(ApiException::class);
        $this->service->getBreachesForAccount('ServerException');
    }

    public function testIfTransferExceptionIsHandled(): void
    {
        $this->expectExceptionMessage('An error occurred while communicating with remote API');
        $this->expectException(ConnectionException::class);
        $this->service->getBreachesForAccount('TransferException');
    }

    public function testIfGuzzleExceptionIsHandled(): void
    {
        $this->expectExceptionMessage('An error occurred while processing this request. Previous exception might contain further information');
        $this->expectException(ApiException::class);
        $this->service->getBreachesForAccount('Exception');
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * HELPERS & MOCKS
     */

    private function createService(): Cilician
    {
        $httpClientMock = HttpClientMock::get();
        $apiKey         = $this->getApiKey();

        return new Cilician(
            new HaveIBeenPwnedApiClient($httpClientMock, $apiKey),
            new PwnedPasswordsApiClient($httpClientMock, $apiKey),
        );
    }

    private function createCachedService(): Cilician
    {
        $httpClientMock = HttpClientMock::get();
        $apiKey         = $this->getApiKey();

        return new Cilician(
            new HaveIBeenPwnedApiClient($httpClientMock, $apiKey),
            new PwnedPasswordsApiClient($httpClientMock, $apiKey),
            CacheMock::get(),
        );
    }

    private function getApiKey(): string
    {
        return 'test-api-key';
    }
}
