<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use GuzzleHttp\Exception\GuzzleException;
use Jgxvx\Cilician\Mock\Service\HttpClientMock;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Service\PwnedPasswordsApiClient
 */
class PwnedPasswordsApiClientTest extends TestCase
{
    private ?PwnedPasswordsApiClient $service = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->service = $this->createService();
    }

    protected function tearDown(): void
    {
        $this->service = null;
        parent::tearDown();
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * TESTS
     */

    public function testIfRangeIsSearched(): void
    {
        $sha1   = \strtoupper(\sha1('12345678'));
        $prefix = \substr($sha1, 0, 5);
        $suffix = \substr($sha1, 5);

        try {
            $response = $this->service->searchRange($prefix);
        } catch (GuzzleException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }

        self::assertEquals(200, $response->getStatusCode());
        self::assertStringContainsString($suffix, $response->getBody()->getContents());
    }

    /* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     * HELPERS & MOCKS
     */

    private function createService(): PwnedPasswordsApiClient
    {
        return new PwnedPasswordsApiClient(HttpClientMock::get(), 'test-api-key');
    }
}
