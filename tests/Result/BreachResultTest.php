<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Exception\InvalidArgumentException;
use Jgxvx\Cilician\Model\BreachModel;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Result\BreachResult
 */
class BreachResultTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Result\BreachResultTestDataProvider::getValidArrayBreaches()
     * @dataProvider \Jgxvx\Cilician\Result\BreachResultTestDataProvider::getValidModelBreaches()
     */
    public function testIfValidBreachesAreSet(array $breaches = []): void
    {
        $result = new BreachResult($breaches);

        $this->assertCount(\count($breaches), $result->getBreaches());

        foreach ($result->getBreaches() as $breach) {
            $this->assertInstanceOf(BreachModel::class, $breach);
        }
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Result\BreachResultTestDataProvider::getInvalidBreaches()
     */
    public function testIfInvalidBreachesAreDetected(array $breaches = []): void
    {
        $this->expectException(InvalidArgumentException::class);
        new BreachResult($breaches);
    }

    public function testIfPwnageIsDetected(): void
    {
        $dataProvider = new BreachResultTestDataProvider();

        $result = new BreachResult();
        $this->assertFalse($result->isPwned());

        $result = new BreachResult($dataProvider->getValidArrayBreaches());
        $this->assertTrue($result->isPwned());

        $result = new BreachResult($dataProvider->getValidModelBreaches());
        $this->assertTrue($result->isPwned());
    }
}
