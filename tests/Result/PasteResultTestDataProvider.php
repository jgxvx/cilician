<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Model\PasteModel;

class PasteResultTestDataProvider
{
    public static function getValidArrayPastes(): array
    {
        $file   = \realpath(__DIR__) . '/../data/pastes_response.json';
        $pastes = \json_decode(\file_get_contents($file), true);

        return [
            [$pastes],
            [[]],
        ];
    }

    public static function getValidModelPastes(): array
    {
        $paste1 = PasteModel::fromArray(['Source' => 'Pastebin']);
        $paste2 = PasteModel::fromArray(['Source' => 'Pastie']);

        return [
            [[$paste1, $paste2]],
        ];
    }

    public static function getInvalidPastes(): array
    {
        $paste1 = '';

        return [
            [[$paste1]],
        ];
    }
}
