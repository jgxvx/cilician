<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

class DataClassResultTestDataProvider
{
    public static function getDataClasses(): array
    {
        $dataClasses = [
            'Account balances',
            'Address book contacts',
            'Age groups',
            'Ages',
        ];

        return [
            [$dataClasses],
        ];
    }
}
