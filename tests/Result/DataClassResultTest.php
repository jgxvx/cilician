<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Result\DataClassResult
 */
class DataClassResultTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Result\DataClassResultTestDataProvider::getDataClasses()
     */
    public function testIfConstructorInjectionWorks(array $dataClasses = []): void
    {
        $result = new DataClassResult($dataClasses);

        $this->assertEquals($dataClasses, $result->getDataClasses());
    }
}
