<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Result\PasswordCheckResult
 */
class PasswordCheckResultTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Result\PasswordCheckResultTestDataProvider::getConstructorParams()
     */
    public function testIfConstructorParamsInjectionWorks(string $password, int $count = 0): void
    {
        $result = new PasswordCheckResult($password, $count);

        $this->assertEquals($password, $result->getPassword());
        $this->assertEquals($count, $result->getCount());
        $this->assertFalse($result->isCached());
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Result\PasswordCheckResultTestDataProvider::getPwnages()
     */
    public function testIfPwnageIsDetected(PasswordCheckResult $result, bool $expected): void
    {
        $this->assertEquals($expected, $result->isPwned());
    }
}
