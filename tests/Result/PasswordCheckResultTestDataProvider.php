<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

class PasswordCheckResultTestDataProvider
{
    public static function getConstructorParams(): array
    {
        return [
            ['12345678', 2000000],
            ['notpwned', 0],
        ];
    }

    public static function getPwnages(): array
    {
        $result1 = new PasswordCheckResult('12345678', 20000000);
        $pwned1  = true;

        $result2 = new PasswordCheckResult('notpwned', 0);
        $pwned2  = false;

        return [
            [$result1, $pwned1],
            [$result2, $pwned2],
        ];
    }
}
