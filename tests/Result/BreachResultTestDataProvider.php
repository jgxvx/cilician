<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Model\BreachModel;

class BreachResultTestDataProvider
{
    public static function getValidArrayBreaches(): array
    {
        $file     = \realpath(__DIR__) . '/../data/breaches_response.json';
        $breaches = \json_decode(\file_get_contents($file), true);

        return [
            [$breaches],
            [[]],
        ];
    }

    public static function getValidModelBreaches(): array
    {
        $breach1 = BreachModel::fromArray(['Name' => 'Foo']);
        $breach2 = BreachModel::fromArray(['Name' => 'Bar']);

        return [
            [[$breach1, $breach2]],
        ];
    }

    public static function getInvalidBreaches(): array
    {
        $breach1 = '';

        return [
            [[$breach1]],
        ];
    }
}
