<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Model\PasteModel;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Result\PasteResult
 */
class PasteResultTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Result\PasteResultTestDataProvider::getValidArrayPastes()
     * @dataProvider \Jgxvx\Cilician\Result\PasteResultTestDataProvider::getValidModelPastes()
     */
    public function testIfValidPastesAreSet(array $pastes = []): void
    {
        $result = new PasteResult($pastes);

        $this->assertCount(\count($pastes), $result->getPastes());

        foreach ($result->getPastes() as $paste) {
            $this->assertInstanceOf(PasteModel::class, $paste);
        }
    }

    /**
     * @dataProvider \Jgxvx\Cilician\Result\PasteResultTestDataProvider::getInvalidPastes()
     */
    public function testIfInvalidPastesAreDetected(array $pastes = []): void
    {
        $this->expectException(\Jgxvx\Cilician\Exception\InvalidArgumentException::class);
        new PasteResult($pastes);
    }

    public function testIfPwnageIsDetected(): void
    {
        $dataProvider = new PasteResultTestDataProvider();

        $result = new PasteResult();
        $this->assertFalse($result->isPwned());

        $result = new PasteResult($dataProvider->getValidArrayPastes());
        $this->assertTrue($result->isPwned());

        $result = new PasteResult($dataProvider->getValidModelPastes());
        $this->assertTrue($result->isPwned());
    }
}
