<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Model\BreachModel
 */
class BreachModelTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Model\BreachModelTestDataProvider::getBreachModels()
     */
    public function testIfConstructorInjectionWorks(array $data): void
    {
        $breachModel = BreachModel::fromArray($data);

        self::assertEquals($data[BreachModelFields::NAME->value], $breachModel->name);
        self::assertEquals($data[BreachModelFields::TITLE->value], $breachModel->title);
        self::assertEquals($data[BreachModelFields::DOMAIN->value], $breachModel->domain);
        self::assertEquals($data[BreachModelFields::PWN_COUNT->value], $breachModel->pwnCount);
        self::assertEquals($data[BreachModelFields::DESCRIPTION->value], $breachModel->description);
        self::assertEquals($data[BreachModelFields::DATA_CLASSES->value], $breachModel->dataClasses);
        self::assertEquals($data[BreachModelFields::IS_VERIFIED->value], $breachModel->isVerified);
        self::assertEquals($data[BreachModelFields::IS_SENSITIVE->value], $breachModel->isSensitive);
        self::assertEquals($data[BreachModelFields::IS_RETIRED->value], $breachModel->isRetired);
        self::assertEquals($data[BreachModelFields::IS_SPAM_LIST->value], $breachModel->isSpamList);
        self::assertEquals($data[BreachModelFields::IS_FABRICATED->value], $breachModel->isFabricated);

        self::assertEquals(\substr($data[BreachModelFields::BREACH_DATE->value], 0, 10), $breachModel->breachDate->format('Y-m-d'));
        self::assertEquals(\substr($data[BreachModelFields::ADDED_DATE->value], 0, 10), $breachModel->addedDate->format('Y-m-d'));
        self::assertEquals(\substr($data[BreachModelFields::MODIFIED_DATE->value], 0, 10), $breachModel->modifiedDate->format('Y-m-d'));
    }
}
