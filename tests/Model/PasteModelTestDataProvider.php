<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

class PasteModelTestDataProvider
{
    public static function getPasteModels(): array
    {
        return [
            [
                [
                    'Source'     => 'Pastebin',
                    'Id'         => '8Q0BvKD8',
                    'Title'      => 'syslog',
                    'Date'       => '2014-03-04T19:14:54Z',
                    'EmailCount' => 139,
                ],
            ],
            [
                [
                    'Source'     => 'Pastie',
                    'Id'         => '7152479',
                    'Date'       => '2013-03-28T16:51:10Z',
                    'EmailCount' => 30,
                ],
            ],
            [
                [
                    'Title'      => null,
                    'Source'     => 'Pastie',
                    'Id'         => '7152479',
                    'Date'       => '2013-03-28T16:51:10Z',
                    'EmailCount' => 30,
                ],
            ],
            [
                [
                    'Title'      => null,
                    'Source'     => null,
                    'Id'         => null,
                    'Date'       => null,
                    'EmailCount' => null,
                ],
            ],
        ];
    }
}
