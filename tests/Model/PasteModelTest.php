<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Jgxvx\Cilician\Model\PasteModel
 */
class PasteModelTest extends TestCase
{
    /**
     * @dataProvider \Jgxvx\Cilician\Model\PasteModelTestDataProvider::getPasteModels()
     */
    public function testIfConstructorInjectionWorks(array $data): void
    {
        $pasteModel = PasteModel::fromArray($data);

        $expectedSource = $data[PasteModelFields::SOURCE->value] ?? '';
        $expectedId     = $data[PasteModelFields::ID->value] ?? '-1';
        $expectedCount  = $data[PasteModelFields::EMAIL_COUNT->value] ?? 0;

        $this->assertEquals($expectedSource, $pasteModel->source);
        $this->assertEquals($expectedId, $pasteModel->id);
        $this->assertEquals($expectedCount, $pasteModel->emailCount);

        if (isset($data[PasteModelFields::TITLE->value])) {
            $expected = $data[PasteModelFields::TITLE->value] ?? '';
            $this->assertEquals($expected, $pasteModel->title);
        }

        if ($data[PasteModelFields::DATE->value]) {
            $this->assertEquals(\substr($data[PasteModelFields::DATE->value], 0, 10), $pasteModel->date->format('Y-m-d'));
        } else {
            $this->assertNull($pasteModel->date);
        }
    }
}
