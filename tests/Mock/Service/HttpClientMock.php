<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Mock\Service;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class HttpClientMock
{
    /**
     * @return ClientInterface|\Mockery\MockInterface
     */
    public static function get()
    {
        $mock = \Mockery::mock(ClientInterface::class);

        $mock->shouldReceive('send')->andReturnUsing(function (Request $request) {
            $uri  = (string) $request->getUri();
            $path = \realpath(__DIR__) . '/../../data';

            switch ($uri) {
                case '/range/7C222':
                    $file     = $path . '/7c222_response.txt';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/range/11AE6':
                    $headers = [
                        'Content-Length' => 0,
                        'Retry-After'    => 2,
                    ];

                    $response = new Response(429, $headers);

                    throw new ClientException('Too many requests', $request, $response);

                    break;
                case '/api/v3/breaches':
                    $file     = $path . '/breaches_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/breaches?domain=adobe.com&includeUnverified=0':
                    $file     = $path . '/breaches_adobe_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/breaches?domain=adsfxyz1234jgxvx.com&includeUnverified=0':
                    $file     = $path . '/breaches_adsfxyz1234jgxvx_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/breach/Adobe':
                    $file     = $path . '/breach_adobe_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/breach/Adobexxx':
                    $response = new Response(200, [], '');

                    break;
                case '/api/v3/breachedaccount/john@doe.com':
                    $file     = $path . '/breaches_account_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/breachedaccount/john@doe.com?domain=adobe.com&includeUnverified=0':
                    $file     = $path . '/breaches_account_adobe_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/breachedaccount/UnknownClientException':
                    throw new ClientException('Null Response', $request, null);

                    break;
                case '/api/v3/breachedaccount/NotFound':
                    $response = new Response(404);

                    throw new ClientException('Not Found', $request, $response);

                    break;
                case '/api/v3/breachedaccount/NotAuthorized':
                    $response = new Response(403);

                    throw new ClientException('Not Authorized', $request, $response);

                    break;
                case '/api/v3/breachedaccount/ServerException':
                    $response = new Response(500);

                    throw new ServerException('ServerException', $request, $response);

                    break;
                case '/api/v3/breachedaccount/TransferException':
                    throw new TransferException('Transfer Exception');

                    break;
                case '/api/v3/breachedaccount/GeneralClientException':
                    $response = new Response(400);

                    throw new ClientException('This is the message', $request, $response);

                    break;
                case '/api/v3/breachedaccount/Exception':
                    throw new \RuntimeException('Some runtime exception');

                    break;
                case '/api/v3/dataclasses':
                    $file     = $path . '/dataclasses_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/pasteaccount/john@doe.com':
                    $file     = $path . '/pastes_response.json';
                    $response = static::respondWithFileContent($file);

                    break;
                case '/api/v3/pasteaccount/jane@doe.com':
                    $response = new Response(404);

                    throw new ClientException('Not Found', $request, $response);

                    break;
                default:
                    $response = new Response(200, []);
            }

            return $response;
        });

        return $mock;
    }

    private static function respondWithFileContent(string $file): Response
    {
        if (\file_exists($file) && \is_readable($file)) {
            $contents = \file_get_contents($file);
        } else {
            $contents = '';
        }

        $headers = [
            'Content-Length' => \strlen($contents),
        ];

        return new Response(200, $headers, $contents);
    }
}
