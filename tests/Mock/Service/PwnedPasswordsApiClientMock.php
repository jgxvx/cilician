<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Mock\Service;

use Jgxvx\Cilician\Service\PwnedPasswordsApiClient;

class PwnedPasswordsApiClientMock
{
    /**
     * @return \Mockery\MockInterface|PwnedPasswordsApiClient
     */
    public static function get()
    {
        $mock = \Mockery::mock(PwnedPasswordsApiClient::class);

        $mock->shouldReceive('searchRange')->andReturnUsing(function (string $prefix) {
            return static::getResponseFromFile($prefix);
        });

        return $mock;
    }

    private static function getResponseFromFile(string $prefix): string
    {
        $path = \realpath(__DIR__) . '/../../data';
        $file = $path . '/' . $prefix . '_response.txt';

        if (\file_exists($file) && \is_readable($file)) {
            return \file_get_contents($file);
        }

        return '';
    }
}
