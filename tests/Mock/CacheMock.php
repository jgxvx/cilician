<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Mock;

use Jgxvx\Cilician\Mock\Exception\InvalidCacheKeyException;
use Jgxvx\Cilician\Service\Cilician;
use Psr\SimpleCache\CacheInterface;

class CacheMock
{
    private static array $cache = [];

    /**
     * @return CacheInterface|\Mockery\MockInterface
     */
    public static function get()
    {
        $mock = \Mockery::mock(CacheInterface::class);

        static::$cache = [];

        $mock->shouldReceive('get')->andReturnUsing(function (string $key) {
            switch ($key) {
                case Cilician::CACHE_PREFIX . 'password.' . \strtoupper(\sha1('exception-get')):
                    throw new InvalidCacheKeyException('Invalid cache key: ' . $key);
                default:
                    return static::getItem($key);
            }
        });

        $mock->shouldReceive('set')->andReturnUsing(function (string $key, $value = null) {
            switch ($key) {
                case Cilician::CACHE_PREFIX . 'password.' . \strtoupper(\sha1('exception-set')):
                    throw new InvalidCacheKeyException('Invalid cache key: ' . $key);
                default:
                    static::setItem($key, $value);
            }

            return true;
        });

        return $mock;
    }

    private static function getItem(string $key): mixed
    {
        return static::$cache[$key] ?? null;
    }

    private static function setItem(string $key, $value): void
    {
        static::$cache[$key] = clone $value;
    }
}
