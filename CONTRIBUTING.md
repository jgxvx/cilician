# Contribution Guidelines

## Contributor Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct][1].
By participating in this project you agree to abide by its terms.

## Workflow

* Fork the project
* Create your bugfix or feature
* Write tests covering your added code
* Run php-cs-fixer (see below)
* Create a merge request

## Coding Guidelines

This project comes with a configuration file and an executable for [php-cs-fixer][2] (.php_cs)
that you can use to (re)format your source code for compliance with this project's coding guidelines:

```bash
$ composer cs-fix
```

To review compliance issues without fixing them, run

```bash
$ composer cs-review
```

## Running the Test Suite

To run Cilician's tests, simply run

```bash
$ composer test
```

## Reporting Issues

Please use the project's own [issue tracker][3] to search for existing issues or to open a new ticket.


[1]: https://gitlab.com/jgxvx/cilician/blob/master/CODE_OF_CONDUCT.md
[2]: https://github.com/FriendsOfPHP/PHP-CS-Fixer
[3]: https://gitlab.com/jgxvx/cilician/issues