<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Util;

final class UserAgent
{
    private const NAME = 'jgxvx/cilician';

    private const VERSION = '2.0.0';

    private const DESCRIPTION = 'haveibeenpwned.com and pwnedpasswords.com API client for PHP';

    public function __toString(): string
    {
        return self::NAME . '. ' . self::DESCRIPTION . '. Version ' . self::VERSION;
    }
}
