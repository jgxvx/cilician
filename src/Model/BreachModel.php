<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

use Jgxvx\Cilician\Exception\InvalidDateException;

readonly class BreachModel
{
    /**
     * @throws InvalidDateException
     */
    public static function fromArray(array $data): self
    {
        return new self(
            self::getFieldValue(BreachModelFields::NAME, $data, ''),
            self::getFieldValue(BreachModelFields::TITLE, $data, ''),
            self::getFieldValue(BreachModelFields::DOMAIN, $data, ''),
            self::getFieldValue(BreachModelFields::DESCRIPTION, $data, ''),
            self::getFieldValue(BreachModelFields::DATA_CLASSES, $data, []),
            self::getFieldDate(BreachModelFields::BREACH_DATE, $data),
            self::getFieldDate(BreachModelFields::ADDED_DATE, $data),
            self::getFieldDate(BreachModelFields::MODIFIED_DATE, $data),
            self::getFieldValue(BreachModelFields::PWN_COUNT, $data, 0),
            self::getFieldValue(BreachModelFields::IS_VERIFIED, $data, false),
            self::getFieldValue(BreachModelFields::IS_FABRICATED, $data, false),
            self::getFieldValue(BreachModelFields::IS_SENSITIVE, $data, false),
            self::getFieldValue(BreachModelFields::IS_RETIRED, $data, false),
            self::getFieldValue(BreachModelFields::IS_SPAM_LIST, $data, false),
        );
    }

    public function __construct(
        public string              $name,
        public string              $title,
        public string              $domain,
        public string              $description,
        public array               $dataClasses,
        public ?\DateTimeImmutable $breachDate,
        public ?\DateTimeImmutable $addedDate,
        public ?\DateTimeImmutable $modifiedDate,
        public int                 $pwnCount,
        public bool                $isVerified,
        public bool                $isFabricated,
        public bool                $isSensitive,
        public bool                $isRetired,
        public bool                $isSpamList,
    ) {
    }

    private static function getFieldValue(BreachModelFields $field, array $data, mixed $default): mixed
    {
        return $data[$field->value] ?? $default;
    }

    /**
     * @throws InvalidDateException
     */
    private static function getFieldDate(BreachModelFields $field, array $data): ?\DateTimeImmutable
    {
        $value = $data[$field->value] ?? null;

        if ($value === null) {
            return null;
        }

        try {
            return new \DateTimeImmutable($data[$field->value]);
        } catch (\Exception $e) {
            $message = \sprintf('Invalid date format for field "%s": %s', $field->value, $value);

            throw new InvalidDateException(
                message: $message,
                previous: $e,
            );
        }
    }
}
