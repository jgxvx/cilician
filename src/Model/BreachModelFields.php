<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

enum BreachModelFields: string
{
    case NAME          = 'Name';
    case TITLE         = 'Title';
    case DOMAIN        = 'Domain';
    case BREACH_DATE   = 'BreachDate';
    case ADDED_DATE    = 'AddedDate';
    case MODIFIED_DATE = 'ModifiedDate';
    case PWN_COUNT     = 'PwnCount';
    case DESCRIPTION   = 'Description';
    case DATA_CLASSES  = 'DataClasses';
    case IS_VERIFIED   = 'IsVerified';
    case IS_FABRICATED = 'IsFabricated';
    case IS_SENSITIVE  = 'IsSensitive';
    case IS_RETIRED    = 'IsRetired';
    case IS_SPAM_LIST  = 'IsSpamList';
}
