<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

use Jgxvx\Cilician\Exception\InvalidDateException;

readonly class PasteModel
{
    /**
     * @throws InvalidDateException
     */
    public static function fromArray(array $data): self
    {
        try {
            return new self(
                self::getFieldValue(PasteModelFields::SOURCE, $data, ''),
                self::getFieldValue(PasteModelFields::ID, $data, '-1'),
                self::getFieldValue(PasteModelFields::TITLE, $data, ''),
                isset($data[PasteModelFields::DATE->value]) ? new \DateTimeImmutable($data[PasteModelFields::DATE->value]) : null,
                self::getFieldValue(PasteModelFields::EMAIL_COUNT, $data, 0),
            );
        } catch (\Exception $e) {
            throw new InvalidDateException(
                message: \sprintf('Invalid date format for field "%s": %s', PasteModelFields::DATE->value, $data[PasteModelFields::DATE->value]),
                previous: $e,
            );
        }
    }

    public function __construct(
        public string              $source,
        public string              $id,
        public string              $title,
        public ?\DateTimeImmutable $date,
        public int                 $emailCount,
    ) {
    }

    private static function getFieldValue(PasteModelFields $field, array $data, mixed $default): mixed
    {
        return $data[$field->value] ?? $default;
    }
}
