<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Model;

enum PasteModelFields: string
{
    case SOURCE      = 'Source';
    case ID          = 'Id';
    case TITLE       = 'Title';
    case DATE        = 'Date';
    case EMAIL_COUNT = 'EmailCount';
}
