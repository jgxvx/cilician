<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Filter;

class BreachFilter
{
    private ?string $domain = null;

    private bool $includeUnverified = false;

    public function __construct(array $data = [])
    {
        $this->init($data);
    }

    public function __toString(): string
    {
        $vars = [
            'domain'            => $this->getDomain(),
            'includeUnverified' => $this->getIncludeUnverified(),
        ];

        $vars = \array_filter($vars, static fn ($value) => $value !== null);

        return \http_build_query($vars);
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getIncludeUnverified(): bool
    {
        return $this->includeUnverified;
    }

    public function setIncludeUnverified(bool $includeUnverified): self
    {
        $this->includeUnverified = $includeUnverified;

        return $this;
    }

    private function init(array $data = []): void
    {
        foreach ($data as $key => $value) {
            $setter = 'set' . \ucfirst($key);

            if (\is_callable([$this, $setter])) {
                $this->$setter($value);
            }
        }
    }
}
