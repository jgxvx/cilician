<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Util\UserAgent;
use Psr\Http\Message\ResponseInterface;

class HaveIBeenPwnedApiClient implements ApiClientInterface
{
    public const BASE_URI = 'https://haveibeenpwned.com';

    private const API_VERSION = '3';

    public function __construct(
        private readonly ClientInterface $http,
        private readonly string $apiKey,
    ) {
    }

    /**
     * @throws GuzzleException
     */
    public function getBreachesForAccount(string $account, BreachFilter $filter = null): ResponseInterface
    {
        $request = new Request(
            'GET',
            \sprintf('%s/breachedaccount/%s?%s', $this->getBasePath(), $account, $filter),
            $this->getDefaultHeaders(),
        );

        return $this->http->send($request);
    }

    /**
     * @throws GuzzleException
     */
    public function getBreachedSites(BreachFilter $filter = null): ResponseInterface
    {
        $request = new Request(
            'GET',
            $this->getBasePath() . '/breaches?' . $filter,
            $this->getDefaultHeaders(),
        );

        return $this->http->send($request);
    }

    /**
     * @throws GuzzleException
     */
    public function getBreachedSite(string $name): ResponseInterface
    {
        $request = new Request('GET', $this->getBasePath() . '/breach/' . $name, $this->getDefaultHeaders());

        return $this->http->send($request);
    }

    /**
     * @throws GuzzleException
     */
    public function getDataClasses(): ResponseInterface
    {
        $request = new Request('GET', $this->getBasePath() . '/dataclasses', $this->getDefaultHeaders());

        return $this->http->send($request);
    }

    /**
     * @throws GuzzleException
     */
    public function getPastesForAccount(string $account): ResponseInterface
    {
        $request = new Request('GET', $this->getBasePath() . '/pasteaccount/' . $account, $this->getDefaultHeaders());

        return $this->http->send($request);
    }

    private function getBasePath(): string
    {
        return \sprintf('/api/v%s', self::API_VERSION);
    }

    private function getDefaultHeaders(): array
    {
        return [
            'user-agent'   => (string) new UserAgent(),
            'hibp-api-key' => $this->apiKey,
        ];
    }
}
