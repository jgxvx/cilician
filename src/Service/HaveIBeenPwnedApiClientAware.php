<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

interface HaveIBeenPwnedApiClientAware
{
    public function getHaveIBeenPwnedApiClient(): HaveIBeenPwnedApiClient;

    public function setHaveIBeenPwnedApiClient(HaveIBeenPwnedApiClient $client): void;
}
