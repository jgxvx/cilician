<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Jgxvx\Cilician\Util\UserAgent;
use Psr\Http\Message\ResponseInterface;

class PwnedPasswordsApiClient implements ApiClientInterface
{
    public const BASE_URI = 'https://api.pwnedpasswords.com';

    public function __construct(
        private readonly ClientInterface $http,
    ) {
    }

    /**
     * @throws GuzzleException
     */
    public function searchRange(string $prefix): ResponseInterface
    {
        $request = new Request('GET', '/range/' . $prefix, $this->getDefaultHeaders());

        return $this->http->send($request);
    }

    private function getDefaultHeaders(): array
    {
        return [
            'user-agent' => (string) new UserAgent(),
        ];
    }
}
