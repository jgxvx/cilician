<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Service;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TransferException;
use Jgxvx\Cilician\Exception\ApiException;
use Jgxvx\Cilician\Exception\AuthorizationException;
use Jgxvx\Cilician\Exception\ConnectionException;
use Jgxvx\Cilician\Exception\GeneralClientException;
use Jgxvx\Cilician\Exception\RateLimitException;
use Jgxvx\Cilician\Exception\ResultException;
use Jgxvx\Cilician\Filter\BreachFilter;
use Jgxvx\Cilician\Result\BreachResult;
use Jgxvx\Cilician\Result\DataClassResult;
use Jgxvx\Cilician\Result\PasswordCheckResult;
use Jgxvx\Cilician\Result\PasteResult;
use Jgxvx\Cilician\Result\Result;
use Jgxvx\Cilician\Util\Cacheable;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class Cilician implements HaveIBeenPwnedApiClientAware, PwnedPasswordsApiClientAware, LoggerAwareInterface
{
    use HaveIBeenPwnedApiClientAwareTrait;
    use PwnedPasswordsApiClientAwareTrait;
    use LoggerAwareTrait;

    public const CACHE_PREFIX = 'ch.jgxvx.cilician.';

    private const DEFAULT_DELAY = 2;

    private ?CacheInterface $cache;

    public function __construct(
        HaveIBeenPwnedApiClient $haveIBeenPwnedApiClient,
        PwnedPasswordsApiClient $pwnedPasswordsApiClient,
        ?CacheInterface $cache = null,
        ?LoggerInterface $logger = null,
    ) {
        $this->setHaveIBeenPwnedApiClient($haveIBeenPwnedApiClient);
        $this->setPwnedPasswordsApiClient($pwnedPasswordsApiClient);
        $this->cache  = $cache;
        $this->logger = $logger;
    }

    public function checkPassword(string $password): PasswordCheckResult
    {
        $sha1       = \strtoupper(\sha1($password));
        $prefix     = \substr($sha1, 0, 5);
        $suffix     = \substr($sha1, 5);
        $cacheKey   = 'password.' . $sha1;
        $cachedItem = $this->getFromCache($cacheKey);

        if ($cachedItem instanceof PasswordCheckResult) {
            return $cachedItem;
        }

        $response = $this->performClientOperation($this->pwnedPasswordsApiClient, 'searchRange', $prefix);
        $data     = $this->getPasswordDataFromResponse($suffix, $response);

        if ($data === null) {
            $result = new PasswordCheckResult($password);
        } else {
            [, $count] = \explode(':', $data);
            $result    = new PasswordCheckResult($password, (int) $count);
        }

        $this->setToCache($cacheKey, $result);

        return $result;
    }

    /**
     * @throws ApiException
     * @throws ResultException
     */
    public function getBreachedSites(BreachFilter $filter = null): BreachResult
    {
        $cacheKey   = 'breached_sites.' . ($filter ? \sha1($filter->__toString()) : '-');
        $cachedItem = $this->getFromCache($cacheKey);

        if ($cachedItem instanceof BreachResult) {
            return $cachedItem;
        }

        $response = $this->performClientOperation($this->haveIBeenPwnedApiClient, 'getBreachedSites', $filter);
        $contents = $response->getBody()->getContents();

        try {
            $result = new BreachResult(\json_decode($contents, true, 512, \JSON_THROW_ON_ERROR));
        } catch (\JsonException $e) {
            throw new ApiException('Invalid JSON response', 0, $e);
        }

        $this->setToCache($cacheKey, $result);

        return $result;
    }

    /**
     * @throws ApiException
     * @throws ResultException
     */
    public function getBreachedSite(string $name): BreachResult
    {
        $cacheKey   = 'breached_site.' . \sha1($name);
        $cachedItem = $this->getFromCache($cacheKey);

        if ($cachedItem instanceof BreachResult) {
            return $cachedItem;
        }

        $response = $this->performClientOperation($this->haveIBeenPwnedApiClient, 'getBreachedSite', $name);
        $contents = $response->getBody()->getContents();

        try {
            $breaches = $contents !== '' ? [\json_decode($contents, true, 512, \JSON_THROW_ON_ERROR)] : [];
        } catch (\JsonException $e) {
            throw new ApiException('Invalid JSON response', 0, $e);
        }
        $result = new BreachResult($breaches);

        $this->setToCache($cacheKey, $result);

        return $result;
    }

    /**
     * @throws ApiException
     * @throws ResultException
     */
    public function getBreachesForAccount(string $account, BreachFilter $filter = null): BreachResult
    {
        $cacheKey   = 'breach_account.' . \sha1($account) . '.' . ($filter ? \sha1($filter->__toString()) : '-');
        $cachedItem = $this->getFromCache($cacheKey);

        if ($cachedItem instanceof BreachResult) {
            return $cachedItem;
        }

        $response = $this->performClientOperation($this->haveIBeenPwnedApiClient, 'getBreachesForAccount', $account, $filter);
        $contents = $response->getBody()->getContents();

        try {
            $breaches = $contents !== '' ? \json_decode($contents, true, 512, \JSON_THROW_ON_ERROR) : [];
        } catch (\JsonException $e) {
            throw new ApiException('Invalid JSON response', 0, $e);
        }
        $result = new BreachResult($breaches);

        $this->setToCache($cacheKey, $result);

        return $result;
    }

    /**
     * @throws ApiException
     */
    public function getDataClasses(): DataClassResult
    {
        $cacheKey   = 'dataclasses';
        $cachedItem = $this->getFromCache($cacheKey);

        if ($cachedItem instanceof DataClassResult) {
            return $cachedItem;
        }

        $response = $this->performClientOperation($this->haveIBeenPwnedApiClient, 'getDataClasses');
        $contents = $response->getBody()->getContents();

        try {
            $classes = $contents !== '' ? \json_decode($contents, true, 512, \JSON_THROW_ON_ERROR) : [];
        } catch (\JsonException $e) {
            throw new ApiException('Invalid JSON response', 0, $e);
        }
        $result = new DataClassResult($classes);

        $this->setToCache($cacheKey, $result);

        return $result;
    }

    /**
     * @throws ApiException
     * @throws ResultException
     */
    public function getPastesForAccount(string $account): PasteResult
    {
        $cacheKey   = 'pasteaccount.' . \sha1($account);
        $cachedItem = $this->getFromCache($cacheKey);

        if ($cachedItem instanceof PasteResult) {
            return $cachedItem;
        }

        $response = $this->performClientOperation($this->haveIBeenPwnedApiClient, 'getPastesForAccount', $account);
        $contents = $response->getBody()->getContents();

        if ($response->getStatusCode() === 200) {
            try {
                $pastes = $contents !== '' ? \json_decode($contents, true, 512, \JSON_THROW_ON_ERROR) : [];
            } catch (\JsonException $e) {
                throw new ApiException('Invalid JSON response', 0, $e);
            }
        } else {
            $pastes = [];
        }

        $result = new PasteResult($pastes);

        $this->setToCache($cacheKey, $result);

        return $result;
    }

    private function getFromCache(string $key): ?Result
    {
        if (!$this->cache) {
            return null;
        }

        try {
            /** @var Cacheable|Result|null $item */
            $item = $this->cache->get(static::CACHE_PREFIX . $key);
        } catch (InvalidArgumentException) {
            $item = null;
        }

        $item?->setCached(true);

        return $item;
    }

    /**
     * @throws ApiException
     * @throws ConnectionException
     * @throws RateLimitException
     * @throws GeneralClientException
     */
    private function performClientOperation(ApiClientInterface $client, string $operation, mixed ...$args): ResponseInterface
    {
        try {
            $response = $client->{$operation}(...$args);
        } catch (ClientException $e) {
            $response = $e->getResponse();

            if ($response === null) {
                throw new GeneralClientException($e->getMessage(), $e->getCode(), $e);
            }

            if ($response->getStatusCode() === 404) {
                return $response;
            }

            if ($response->getStatusCode() === 429) {
                $header = $response->getHeader('Retry-After');
                $delay  = \count($header) ? (int) $header[0] : self::DEFAULT_DELAY;

                $this->logError('Rate limit hit', ['delay' => $delay]);

                throw new RateLimitException($delay);
            }

            if ($response->getStatusCode() === 403) {
                throw new AuthorizationException('You are not authorized to make this request.', $e->getCode(), $e);
            }

            throw new GeneralClientException($e->getMessage(), $e->getCode(), $e);
        } catch (ServerException $e) {
            $this->logError($e->getMessage(), ['operation' => $operation]);

            throw new ApiException('An error occurred on the remote API server', 0, $e);
        } catch (TransferException $e) {
            $this->logError($e->getMessage(), ['operation' => $operation]);

            throw new ConnectionException('An error occurred while communicating with remote API', 0, $e);
        } catch (\Throwable $ex) {
            $this->logError($ex->getMessage(), ['operation' => $operation]);

            throw new ApiException(
                'An error occurred while processing this request. Previous exception might contain further information',
                0,
                $ex->getPrevious(),
            );
        }

        return $response;
    }

    private function getPasswordDataFromResponse(string $suffix, ResponseInterface $response): ?string
    {
        $contents = $response->getBody()->getContents();
        $pos      = \strpos($contents, $suffix);

        if ($pos === false) {
            return null;
        }

        $sub = \substr($contents, $pos);

        return \substr($sub, 0, \strpos($sub, \PHP_EOL));
    }

    private function setToCache(string $cacheKey, Cacheable $result): bool
    {
        if ($this->cache === null) {
            return false;
        }

        try {
            $set = $this->cache->set(static::CACHE_PREFIX . $cacheKey, $result);
        } catch (InvalidArgumentException $e) {
            $this->logError($e->getMessage(), ['cache_key' => $cacheKey]);
            $set = false;
        }

        return $set;
    }

    private function logError(string $message, array $context): void
    {
        $this->logger?->error($message, $context);
    }
}
