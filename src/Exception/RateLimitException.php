<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Exception;

class RateLimitException extends \RuntimeException implements CilicianException
{
    private int $delay;

    public function __construct(int $seconds, int $code = 0, \Throwable $previous = null)
    {
        $this->delay = $seconds;

        $message = \sprintf('Rate limit exceeded. Wait for %d second(s).', $seconds);

        parent::__construct($message, $code, $previous);
    }

    public function getDelay(): int
    {
        return $this->delay;
    }
}
