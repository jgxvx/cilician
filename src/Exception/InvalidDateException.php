<?php
/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
declare(strict_types=1);
namespace Jgxvx\Cilician\Exception;

class InvalidDateException extends \Exception implements CilicianException
{
}
