<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Util\PwnageAware;

class PasswordCheckResult extends AbstractResult implements PwnageAware
{
    private string $password;

    private int $count;

    public function __construct(string $password, int $count = 0)
    {
        $this->password = $password;
        $this->count    = $count;
    }

    public function isPwned(): bool
    {
        return $this->count > 0;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getCount(): int
    {
        return $this->count;
    }
}
