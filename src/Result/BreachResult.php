<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Exception\InvalidArgumentException;
use Jgxvx\Cilician\Exception\InvalidDateException;
use Jgxvx\Cilician\Exception\ResultException;
use Jgxvx\Cilician\Model\BreachModel;
use Jgxvx\Cilician\Util\PwnageAware;

class BreachResult extends AbstractResult implements PwnageAware
{
    /** @var array<BreachModel> */
    private array $breaches = [];

    /**
     * @throws ResultException
     */
    public function __construct(array $breaches = [])
    {
        foreach ($breaches as $breach) {
            if (\is_array($breach)) {
                try {
                    $this->breaches[] = BreachModel::fromArray($breach);
                } catch (InvalidDateException $e) {
                    throw new ResultException(
                        'Invalid breach date: ' . $e->getMessage(),
                        $e->getCode(),
                        $e,
                    );
                }
            } elseif ($breach instanceof BreachModel) {
                $this->breaches[] = $breach;
            } else {
                throw new InvalidArgumentException('Invalid breach: ' . \gettype($breach));
            }
        }
    }

    public function isPwned(): bool
    {
        return \count($this->breaches) > 0;
    }

    /**
     * @return array<BreachModel>
     */
    public function getBreaches(): array
    {
        return $this->breaches;
    }
}
