<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

use Jgxvx\Cilician\Exception\InvalidArgumentException;
use Jgxvx\Cilician\Exception\InvalidDateException;
use Jgxvx\Cilician\Exception\ResultException;
use Jgxvx\Cilician\Model\PasteModel;
use Jgxvx\Cilician\Util\PwnageAware;

class PasteResult extends AbstractResult implements PwnageAware
{
    /** @var array<PasteModel> */
    private array $pastes = [];

    /**
     * @param array<array|PasteModel> $pastes
     *
     * @throws ResultException
     */
    public function __construct(array $pastes = [])
    {
        foreach ($pastes as $paste) {
            if (\is_array($paste)) {
                try {
                    $this->pastes[] = PasteModel::fromArray($paste);
                } catch (InvalidDateException $e) {
                    throw new ResultException(
                        'Invalid paste date: ' . $e->getMessage(),
                        $e->getCode(),
                        $e,
                    );
                }
            } elseif ($paste instanceof PasteModel) {
                $this->pastes[] = $paste;
            } else {
                throw new InvalidArgumentException('Invalid paste: ' . \gettype($paste));
            }
        }
    }

    public function isPwned(): bool
    {
        return \count($this->pastes) > 0;
    }

    /**
     * @return array<PasteModel>
     */
    public function getPastes(): array
    {
        return $this->pastes;
    }
}
