<?php

/*
 * This file is part of jgxvx/cilician.
 *
 * (c) Jürg Gutknecht <info@jgxvx.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */
namespace Jgxvx\Cilician\Result;

class DataClassResult extends AbstractResult
{
    /** @var array<string> */
    private array $dataClasses;

    public function __construct(array $dataClasses)
    {
        $this->dataClasses = $dataClasses;
    }

    /**
     * @return array<string>
     */
    public function getDataClasses(): array
    {
        return $this->dataClasses;
    }
}
