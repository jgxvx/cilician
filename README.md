# Cilician

**Cilician** is a PHP client for [haveibeenpwned.com][1] APIs ([v3][23]).

[![pipeline status](https://gitlab.com/jgxvx/cilician/badges/main/pipeline.svg)](https://gitlab.com/jgxvx/cilician/commits/master)
[![coverage report](https://gitlab.com/jgxvx/cilician/badges/main/coverage.svg?job=test)](https://gitlab.com/jgxvx/cilician/commits/master)


## Installation

The recommended way to install Cilician is by using [Composer][6]:

```console
$ composer require "jgxvx/cilician"
```


## Getting Started

Ideally, the Cilician service is used to interact with the APIs. It is an abstraction layer for the specific API clients and will return convenient result objects or throw exceptions in case of failures.

Following the principle of [dependency injection][8], the Cilician service is passed two API client objects and, optionally, a [PSR-16][7] compliant cache implementation.

Both API clients expect a [Guzzle HTTP client][9] as their constructor parameter. The `HaveIBeenPwnedApiClient` also expects a [Have I Been Pwned API key][22].

```php
$hibpClient = new HaveIBeenPwnedApiClient(new \GuzzleHttp\Client(['base_uri' => HaveIBeenPwnedApiClient::BASE_URI]), 'your-api-key');
$ppClient   = new PwnedPasswordsApiClient(new \GuzzleHttp\Client(['base_uri' => PwnedPasswordsApiClient::BASE_URI]));
$cilician   = new Cilician($hibpClient, $ppClient);
```

### Checking Passwords

To check if a password has appeared in a data breach:

```php
$result = $cilician->checkPassword('12345678');

if ($result->isPwned()) {
    // Handle failure case...
    echo "The password you've chosen has appeared {$result->getCount()} times in data breaches. Do not use it.";
} else {
    // Handle success case...
    echo "All good!";
}
```

[API Documentation][10]


### Getting All Breaches for an Account

To retrieve a list of all breaches a particular account has been involved in:

```php
$result = $cilician->getBreachesForAccount('john@doe.com');

if ($result->isPwned()) {
    echo "Your account has appeared in the following breaches:" . PHP_EOL;
    
    foreach ($result->getBreaches() as $breach) {
        echo $breach->getName() . PHP_EOL;
    }
}
```

The `getBreachesForAccount` method returns an array of [BreachModel][12] instances -- value objects representing the API's [breach model][13].

To narrow down the list, a filter object can be passed as a second argument:

```php
$filter = new BreachFilter(['domain' => 'adobe.com']);
$result = $cilician->getBreachesForAccount('john@doe.com', $filter);
```

[API Documentation][11]


### Getting All Breached Sites

To retrieve a list of all breached sites:

```php
$result = $cilician->getBreachedSites();

foreach ($result->getBreaches() as $breach) {
    echo $breach->getName() . PHP_EOL;
}
```

To narrow down the list, a filter object can be passed to the `getBreachedSites` method:

```php
$filter = new BreachFilter(['domain' => 'adobe.com']);
$result = $cilician->getBreachedSites($filter);
```

[API Documentation][14]


### Getting a Single Breached Site

To retrieve a single breached site:

```php
$result = $cilician->getBreachedSite('Adobe');

if ($result->isPwned()) {
    $breach = $result->getBreaches()[0]; // Safe, as isPwned() only returns true if there is at least one breach.
}
```

[API Documentation][15]


### Getting All Available Data Classes

To retrieve all data classes:

```php
$result = $cilician->getDataClasses();

foreach ($result->getDataClasses() as $dataClass) {
    echo $dataClass . PHP_EOL; // Data classes are strings
}
```

[API Documentation][16]


### Getting All Pastes for an Account

To retrieve all pastes for an account:

```php
$result = $cilician->getPastesForAccount('john@doe.com');

foreach ($result->getPastes() as $paste) {
    echo $paste->getName() . PHP_EOL;
}
```

The `getPastesForAccount` method returns an array of [PasteModel][18] instances -- value objects representing the API's [paste model][19].

[API Documentation][17]


### Filtering

When searching for breaches, a filter can specified to narrow down the search.

Currently, the breach filter supports two parameters:

* `domain` -- A domain name string. Default is `null`.
* `includeUnverified` -- Whether unverified breaches should be included. Default is `false`.

The parameters can either be passed to the filter's constructor as an array, or set by setter methods:

```php
$filter = new BreachFilter(['domain' => 'adobe.com', 'includeUnverified' => true]);

// or

$filter = new BreachFilter();
$filter
    ->setDomain('adobe.com')
    ->setIncludeUnverified(true);
```

### Caching

If the application uses a PSR-16 compliant caching system, the cache can be passed to Cilician as a third constructor parameter.
Cilician will then automatically cache responses using the caching system's default TTL.

If a [PSR-6][21] compliant cache is used, it can be converted to PSR-16 with [php-cache/simple-cache-bridge][20] or similar.

## License

Cilician is open source software published under the MIT license. Please refer to the [LICENSE][4] file for further information.


## Contributing

We appreciate your help! To contribute, please read the [Contribution Guidelines][3] and our [Code of Conduct][5].



[1]: https://haveibeenpwned.com
[2]: https://pwnedpasswords.com
[3]: https://gitlab.com/jgxvx/cilician/blob/master/CONTRIBUTING.md
[4]: https://gitlab.com/jgxvx/cilician/blob/master/LICENSE
[5]: https://gitlab.com/jgxvx/cilician/blob/master/CODE_OF_CONDUCT.md
[6]: https://getcomposer.org
[7]: https://www.php-fig.org/psr/psr-16/
[8]: https://en.wikipedia.org/wiki/Dependency_injection
[9]: https://github.com/guzzle/guzzle
[10]: https://haveibeenpwned.com/API/v2#PwnedPasswords
[11]: https://haveibeenpwned.com/API/v2#BreachesForAccount
[12]: https://gitlab.com/jgxvx/cilician/blob/master/src/Model/BreachModel.php
[13]: https://haveibeenpwned.com/API/v2#BreachModel
[14]: https://haveibeenpwned.com/API/v2#AllBreaches
[15]: https://haveibeenpwned.com/API/v2#SingleBreach
[16]: https://haveibeenpwned.com/API/v2#AllDataClasses
[17]: https://haveibeenpwned.com/API/v2#PastesForAccount
[18]: https://gitlab.com/jgxvx/cilician/blob/master/src/Model/PasteModel.php
[19]: https://haveibeenpwned.com/API/v2#PasteModel
[20]: https://github.com/php-cache/simple-cache-bridge
[21]: https://www.php-fig.org/psr/psr-6/
[22]: https://haveibeenpwned.com/API/Key
[23]: https://haveibeenpwned.com/api/v3
